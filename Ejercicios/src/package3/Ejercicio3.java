
package package3;

public class Ejercicio3 {
	public static void main(String []args){
		System.out.println("tecla de escape \t  significado");
		System.out.println("\n");
		System.out.println("\\n \t \t \t  significa nueva l�nea");
		System.out.println("\\t \t \t \t  significa un tab de espacio");
		System.out.println("\\\" \t \t \t  es para poner \" (comillas dobles) dentro del texto por ejemplo \n \t \t \t  \"Belencita\"");
		System.out.println("\\\\ \t \t \t  se utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\");
		System.out.println("\\\' \t \t \t  se utiliza para las \'(comilla simple) para escribir por ejemplo \'Princesita\'");
	}
}
